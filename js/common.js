$(document).ready(function(){
    
    /*  SLIDER   */
    
    $('.reviews').owlCarousel({
        loop: true,
        dots: false,
        margin: 0,
        autoplayTimeout:6000,
        autoplayHoverPause:true,
        nav: false,
        autoplay: true,
        smartSpeed: 500,
        responsive: {
            0: {
                items: 1
            }
        }
    });    
    
    
    /*  country  select   */
    
    $(".country").change(function(){
       
        var ua = ["1798", " грн.", "899"];
        var rus = ["3198", "руб.", "1599"];
        
        if( $(this).val() == 1){ 
            var oldGrnText = ua[0] + "<span>" + ua[1] + "</span>";
            var newGrnText = ua[2] + "<span>" + ua[1] + "</span>";
            $(".old-price-value").html(oldGrnText);
            $(".new-price-value").html(newGrnText);            
        };
        if($(this).val() == 2){             
            var oldrusText = rus[0] + "<span>" + rus[1] + "</span>";
            var newrusText = rus[2] + "<span>" + rus[1] + "</span>";
            $(".old-price-value").html(oldrusText);
            $(".new-price-value").html(newrusText);

        };
//        
    });
    
    /*    Timer   */
    
    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        return {
            'total': t,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    
    
    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
    var now = new Date();

    var tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);

    var diff = tomorrow - now; 
   
    var deadline = new Date(Date.parse(new Date()) + diff); // for endless timer
    
    initializeClock('clockdiv', deadline);
    initializeClock('clockdiv2', deadline);
    
    
    /*  year */
    var FullYear = now.getFullYear();
    $("#nowYear").html(FullYear);
    
    
    /* scroll to element */
 
  
    $('#buy').click( function(){ 
        var scroll_el = $(this).attr('href'); 
        if ($(scroll_el).length != 0) { 
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 600); 
        }
        return false; 
    });
    
    
    
    $(document).scroll(function () {
        var s_top = $("html").scrollTop() + $( window ).height() - 200;
        var yes = $(".usebox").offset().top;
        var no = yes + $( window ).height();
        
        
        if(s_top > yes && no > s_top){
            $("#myVideo2").get(0).play();
            
        }
        else{
            $("#myVideo2").get(0).pause();
        }
        
        var s_top2 = $("html").scrollTop() + $( window ).height() - 300;
        var yes2 = $(".video-container").offset().top;
        var no2 = yes2 + $( window ).height() - 200;
        
        
        if(s_top2 > yes2 && no2 > s_top2){
            $("#myVideo").get(0).play();
        }
        else{
            $("#myVideo").get(0).pause();
        }
        
    });
        
    
});